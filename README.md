<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

        - [](#markdown-header-)
- [Aurelia Fundamentals](#markdown-header-aurelia-fundamentals)
    - [Course Overview](#markdown-header-course-overview)
        - [Quick Intro to ES2016/2016](#markdown-header-quick-intro-to-es20162016)
            - [Promises](#markdown-header-promises)
    - [Getting Started with Aurelia](#markdown-header-getting-started-with-aurelia)
        - [Browser Compatibility](#markdown-header-browser-compatibility)
        - [Aurelia Features](#markdown-header-aurelia-features)
        - [Aurelia Bootstrapping Process](#markdown-header-aurelia-bootstrapping-process)
        - [Customizing App Startup](#markdown-header-customizing-app-startup)
    - [Implementing MVVM with Aurelia](#markdown-header-implementing-mvvm-with-aurelia)
        - [Separation of Concerns](#markdown-header-separation-of-concerns)
        - [Goals & Benefits](#markdown-header-goals-&-benefits)
        - [Architecture](#markdown-header-architecture)
        - [Responsibilities](#markdown-header-responsibilities)
            - [Model](#markdown-header-model)
            - [View](#markdown-header-view)
            - [View Model](#markdown-header-view-model)
                - [Expose](#markdown-header-expose)
                - [Wrapped](#markdown-header-wrapped)
                - [Client State](#markdown-header-client-state)
            - [Client Services](#markdown-header-client-services)
        - [Approaches](#markdown-header-approaches)
        - [Using Compose Element to setup an MVVM Hierarchy](#markdown-header-using-compose-element-to-setup-an-mvvm-hierarchy)
            - [Require Element](#markdown-header-require-element)
            - [Compose Element](#markdown-header-compose-element)
        - [Override View Resolution Conventions](#markdown-header-override-view-resolution-conventions)
    - [Dependency Injection (DI)](#markdown-header-dependency-injection-di)
        - [Purpose of DI and Related patterns](#markdown-header-purpose-of-di-and-related-patterns)
        - [DI in Aurelia](#markdown-header-di-in-aurelia)
        - [Declaratively Registering Lifetime](#markdown-header-declaratively-registering-lifetime)
        - [Registering Explicitly with Container](#markdown-header-registering-explicitly-with-container)
        - [Injecting Resolvers](#markdown-header-injecting-resolvers)
            - [All.of example for Plugins](#markdown-header-allof-example-for-plugins)
        - [Globally Registering Dependencies](#markdown-header-globally-registering-dependencies)
    - [Aurelia Routing Fundamentals](#markdown-header-aurelia-routing-fundamentals)
        - [Routing in Aurelia](#markdown-header-routing-in-aurelia)
        - [Routing Parameters](#markdown-header-routing-parameters)
            - [Consuming Routes in View Models](#markdown-header-consuming-routes-in-view-models)
        - [Generating Route URLs from Routing Configuration](#markdown-header-generating-route-urls-from-routing-configuration)
        - [Using Query String Parameters](#markdown-header-using-query-string-parameters)
        - [Programmatic Controlling Navigation](#markdown-header-programmatic-controlling-navigation)
    - [Routing Beyond the Basics](#markdown-header-routing-beyond-the-basics)
        - [Screen Activation Lifecycle](#markdown-header-screen-activation-lifecycle)
        - [Sibling Navigation Panes with ViewPorts](#markdown-header-sibling-navigation-panes-with-viewports)
        - [Child (Nested) Routers](#markdown-header-child-nested-routers)
        - [Activation Strategy](#markdown-header-activation-strategy)
        - [Push State](#markdown-header-push-state)
        - [Navigation Pipeline](#markdown-header-navigation-pipeline)
        - [Custom Navigation Pipeline Steps](#markdown-header-custom-navigation-pipeline-steps)
    - [Data Binding Fundamentals](#markdown-header-data-binding-fundamentals)
        - [Overview](#markdown-header-overview)
        - [Adaptive Data Binding System](#markdown-header-adaptive-data-binding-system)
        - [Primary Data Binding Syntax](#markdown-header-primary-data-binding-syntax)
        - [Binding with innerHTML and textContent](#markdown-header-binding-with-innerhtml-and-textcontent)
        - [Data Binding Modes: Controlling Data Flow](#markdown-header-data-binding-modes-controlling-data-flow)
        - [Event Bindings Overview](#markdown-header-event-bindings-overview)
    - [Data Binding Beyond the Basics](#markdown-header-data-binding-beyond-the-basics)
        - [Behind the Curtain of Data Binding](#markdown-header-behind-the-curtain-of-data-binding)
        - [Specialized Bindings](#markdown-header-specialized-bindings)
        - [Advanced repeat.for Capabilities](#markdown-header-advanced-repeatfor-capabilities)
        - [Style / CSS Binding](#markdown-header-style-css-binding)
        - [Working with <select>](#markdown-header-working-with-select)
        - [Checkboxes and Radio Buttons](#markdown-header-checkboxes-and-radio-buttons)
        - [Binding with Value Converters](#markdown-header-binding-with-value-converters)
        - [Extending Data Binding with Binding Behaviours](#markdown-header-extending-data-binding-with-binding-behaviours)
            - [Built-in Binding Behaviours](#markdown-header-built-in-binding-behaviours)
            - [Custom Binding Behaviours](#markdown-header-custom-binding-behaviours)
    - [Extending Aurelia with Custom Elements and Attributes](#markdown-header-extending-aurelia-with-custom-elements-and-attributes)
        - [Custom Elements](#markdown-header-custom-elements)
        - [Turning a View and ViewModel into a Custom Element](#markdown-header-turning-a-view-and-viewmodel-into-a-custom-element)
        - [Custom Elements Lifecycle](#markdown-header-custom-elements-lifecycle)
        - [Controlling the Name and Container of a Custom Element](#markdown-header-controlling-the-name-and-container-of-a-custom-element)
        - [Implementing Replacable Parts in a Custom Element](#markdown-header-implementing-replacable-parts-in-a-custom-element)
        - [Custom Attributes](#markdown-header-custom-attributes)
            - [Template Controller](#markdown-header-template-controller)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Aurelia Fundamentals

> My course notes from Pluralsight [course](https://app.pluralsight.com/library/courses/aurelia-fundamentals/table-of-contents)

## Course Overview

Aurelia is a framework for building JavaScript single page applications.
It's written with ES2015, and a few ES2016 features that are not yet approved.

### Quick Intro to ES2016/2016

Important features to understand for developing Aurelia applications:

* Classes
    - constructor
    - getters and setters
    - methods
    - can inherit from other classes
* Modules
    - treating a single js file as its own encapsulation boundary
    - can define multiple classes, variables, functions etc, and decide which ones to expose
    - solves the global namespace issue
    - can export multiple items (functions, variables, classes, etc.)
    - import identifiers become local variables that can be used anywhere in the module
* let variables
    - block scoped rather than `var` which is function scoped
* String templates (sometimes called string interpolation)
* Promises
    - native feature of the language, no longer need an external library
* Decorators and Class Properties

#### Promises

Example, simulate an async function with timeout, after 5 seconds, it returns something:

```javascript
startEngine() {
  setTimeout(function() {
    return 'Roar!';
  }, 5000);
}
```

But because its async, return would never get to caller, eg `let result = startEngine();`.

Before promises, callbacks were used to solve this, caller would have to pass in a callback function, but this gets messy with nested callbacks and error handling:

```javascript
startEngine(callback) {
  setTimeout(function() {
    callback('Roar'!);
  }, 5000)
}
```

New way to do it is with promises. The new `Promise` object type is part of ES2015.
To use it, new up an instance of a Promise, and pass it a function that takes two arguments, a `resolve`, and a `reject` argument.
The resolve and reject arguments will be passed to your code by the runtime.

`resolve` parameter is a function reference to invoke when async work is done, and pass it the result as a parameter.
`reject` parameter is a function reference to invoke when an error occurs in async operation, can be used to pass back the error object.

Finally, the promise must be returned from the function:

```javascript
startEngine() {
  let promise = new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve('Roar!');
    }, 5000);
  });
  return promise;
}

export startEngine;
```

To consume the promise as a caller:

```javascript
import {startEngine} from './somewhere';

someConsumer() {
  startEngine.then(function(result) {
    // do something with result
  });
}
```

## Getting Started with Aurelia

Aurelia is a framework for building loosely coupled, maintainable, well factored single page applications that leverage the latest advancements in the JavaScript language and design patterns.

* Rich interactive client JavaScript applications
* Run in the browser
* Use the Model-View-ViewModel (MVVM) pattern
* Cnvention over configuration
* Leverage the power of the modern browser
* Pluggable architecture

### Browser Compatibility

Designed for evergreen browsers that auto update with latest features. Also supports IE9 and 10 with some polyfills.

### Aurelia Features

* Modular Architecture: Leverages ES2015 modules for defining each of the components in application including view models, services, custom elements and attributes.
* Dependency Injection
* Two-Way Data Binding
* UI Composition (MVVM)
* Routing / Navigation
* Task Queue: queue up multiple asynchronous tasks in a particular order, used by Aurelia internally
* Pub-Sub Messaging: loosely coupled communication between components
* HTML Templating: parse html fragments, finds data binding expressions, custom elements and attributs, and compiles for fast execution every time view is rendered
* Custom Elements: create your own domain-specific custom elements
* Custom Attributes: create your own attributes to add behaviours to existing or custom elements
* Logging

### Aurelia Bootstrapping Process

Convention over configuration. Aurelia defines a number of conventions for what it will do by default, when you don't tell it otherwise. One convention is that when app is initializing, it will look for a module named "app".

When browser encounters `<body aurelia-app>` in index.html, it doesn't know what that is so will ignore it.
But when `System.import('aurelia-bootstrapper');` is run, now there is a path of execution to get things kicked off.

Looking at the [bootstrapper source on github](https://github.com/aurelia/bootstrapper/blob/master/src/index.js#L100), the `run()` method is invoked, which finds all elements with `aurelia-app` attribute, and bootstraps an aurelia app.

### Customizing App Startup

In `index.html`, add `<body aurelia-app="main">`. This is telling Aurelia to use a module named "main" to configure and run the app.

Then create `src/main.js`. By convention, Aurelia will look for function named `configure` to be exported from main.js. This function takes an argument that is the aurelia framework object for the app that is being created by the bootstrapper.

For example, to change the default module loaded from 'app' to your app name, eg: 'hello', set a different module name as root in the `configure` function. Can also specify which element in the dom is the root.

```javascript
export function configure(aurelia) {
  aurelia.use.standardConfiguration();
  aurelia.start().then(a => a.setRoot('hello'));
}
```

## Implementing MVVM with Aurelia

### Separation of Concerns

Example spaghetti code with no separation of concerns, combines structure of what user will see on page (html markup), interaction logic, which itself is interleaved with the structure, and is also mixed up with UI element access:

![No separation of concerns](images/no-separation-concerns.png "No separation of concerns")

Example good separation of concerns using Aurelia. Left hand side contains view, which is structure of what user will see on screen, with a few declarative attributes on those elements, such as `class.bind`, `repeat.for`, `value.bind`, `click.delegate`. These attributes are bindings that provide "glue" for view model, so they can work in conjunction with view model, but in a loosely coupled way.

Right hand side contains view model, bindings relate to exposed properties of view model. Interaction logic is seprated into methods in view model. View model has no knowledge of the dom elements making up the view. View model focuses only on data and state associated with the application, and the use case user is in the middle of. Data bindings turn those into changes in the UI for user to see.

![Good separation of concerns](images/good-separation-concerns.png "Good separation of concerns")

### Goals & Benefits

* Maintainability - more obvious where to go in the code to fix a bug or add a new feature.
* Testability - much easier to write unit tests against interaction logic when its cleanly separated from dom elements.
* Extensibility - easier to integrate new features

### Architecture

Architecture for structuring client side code.

![MVVM architecture](images/mvvm-architecture.png "MVVM architecture")

Its a derivate of the MVC pattern (Model-View-Controller), primary difference being in MVVM there is an ongoing interaction between the view and view model (i.e. they share the same lifetime and are designed to support eachother), whereas in MVC, view and controller may have different lifetimes.

Another unique characteristic of MVVM is data binding. With MVC/MVP communication between views and controllers (or presenters) is typically done through method calls, whereas MVVM is designed around having a rich data binding system, and being able to setup loosely coupled relationships between elements in the view and properties in the view model for data flow and communications back and forth.

### Responsibilities

The layers of MVVM architecture are as follows:

![MVVM layers](images/mvvm-layers.png "MVVM layers")

#### Model

* Contain client data
* Expose rleationships between model objects
* Computed properties (value determined by combination of other properties on that model object or by something computed at runtime based on state in model)
* Typically not much behaviour, but can have embedded validation

#### View

* Structural definition of what user sees on the screen, can be static or dynamic like animations
* Bindings to properties and methods in the view model, this is the communication path between view and view model

#### View Model

* Expose data to the view for presentation and manipulation, including logic for retrieving data and how to persist it if its changed.
* Encapsulate interaction logic (calls to back end, navigation, state transformation - i.e. the way the view wants to see a property might be different than how its stored in the model)

Provides data to view in several different ways.

##### Expose

View model can expose a model object directly. In this case, data bindings can work directly against that model. Bindings can reach into its individual properties and manipulate its state directly.

```javascript
export class MyViewModel {
  constructor() {
    // expose
    this.customer = ...;
  }
}
```

##### Wrapped

Properties exposed by view model may not be one-to-one with a single model object or a single property on one of the model objects. Property on view model whose value is determined by data that's stored somewhere in the model.

![View model data wrapped](images/view-model-data-wrapped.png "View model data wrapped")

##### Client State

Data about execution context in client that needs to drive user experience, such as whether user is logged in or not to determine which views or actions should be enabled in UI.

![View model data client state](images/view-model-data-client-state.png "View model data client state")

#### Client Services

* Shared functionality or data access
* Consumed by one or more view models
* Decouples view models from external dependencies such as data storage, service access, client environment
* Can act as data caching container

### Approaches

* Compose Element: Point to a view and a view model and pull those in as a chunk of UI into a containing view.
* Routing & Navigation: Navigation system can be used to pull a view into a container and the views swap out and their view models hooked up automatically by the routing system.
* Custom Elements: Can define a custom element view that defines structure and view model that defines behaviour.

### Using Compose Element to setup an MVVM Hierarchy

![Community App Page Structure](images/community-app-page-structure.png "Community App Page Structure")

Ignoring header/navigation for now, will come back to this later.

Main view is "shell", which contains two child views: EventsList and SponsorList.

EventsList is a composite view rendering a series of child views, one for each event.

#### Require Element

`<require>` element lets you pull in contents from a module that's been loaded, for example:

```
// main.js
import 'bootstrap';

// shell.html
<template>
  <require from="bootstrap/css/bootstrap.css"></require>
</template>
```

#### Compose Element

`<compose>` lets you specify a view model, which points to a module name. Aurelia will create an instance of the class defined in that module and by convention, will load a corresponding view that has the same name and is in the same folder.

```html
<compose view-model="events"></compose>
```

Another option is to only specify a view, then Aurelia will just render the view template, and not instantiate a view model class. In this case, there will still be a binding context that is a view model, but it will be the parents' view model.

```html
<compose view="events.html"></compose>
```

Can also specify _both_ a view and view model:

```html
<compose view="events" view...></compose>
```

`repeat.for` binding expression allows you to point to a property on a view model (a collection), and can loop over it, such that a new element will be created for each item in the collection, for example:

```html
<div repeat.for="event of events">
  <compose model.bind="event" view-model="event"></compose>
</div>
```

`model.bind` attribute on `<compose>` element allows you to pass in context into the view model. To receive this context in the target view model, use `activate` method, which Aurelia will call as part of the view model activation lifecycle:

```javascript
export class Event {
  constructor() { ... }
  activate(bindingContext) {
    this.event = bindingContext;
  }
}
```

### Override View Resolution Conventions

By convention, when Aurelia resolves a view based on a view model, it assumes they have the same name and are located in the same folder. To have views located in a different folder than view models, need to _override the view locator convention_. This is done in `main.js`.

```javascript
import {ViewLocator} from 'aurelia-framework';

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging();

    ViewLocator.prototype.convertOriginToViewUrl = origin => {
      let moduleId = origin.moduleId;
      let id = (moduleId.endsWith('.js') || moduleId.endsWith('.ts')) ?
        moduleId.substring(0, moduleId.length -3) : moduleId;
      return id.replace('viewmodels', 'views') + '.html';
    }

    aurelia.start().then(a => a.setRoot('viewmodels/shell'));
}
```

## Dependency Injection (DI)

### Purpose of DI and Related patterns

DI is mostly about having loose coupling between dependent components. Related patterns include:

* DI: Concerned with _how_ you get a dependent object reference.
* Inversion of Control (IOC): Handing over responsibility of constructing objects to something besides your own code, typically called a _container_, which constructs an object and hands back a reference.
* Service Locator: Decoupling a consumer of an object reference from how that object reference is actually created (closely related to IOC).

With IoC/DI, a container is responsible for construction of other objects (IoC) and injecting the other objects into the components that depend on them (DI).

Objects to be constructed can declare to the container what kind of lifetime they should have, for example, singleton if there should only be one instance per container.

### DI in Aurelia

When declaring a dependency on some class, first need to make sure its accessible to the consuming code. This is done with module `import`:

```javascript
import {SomeService} from '../path/to/someService';
import {inject} from 'aurelia-framework';
```

Then need to declare on the consuming class that it has a dependency. This is done with `@inject` decorator.

```javascript
import {SomeService} from '../path/to/someService';
import {inject} from 'aurelia-framework';

@inject(SomeService)
export class App {
  ...
}
```

Then Aurelia's dependency injection container will create an instance of that class from the module and pass a reference to that instance to the consuming class' constructor:

```javascript
import {SomeService} from '../path/to/someService';
import {inject} from 'aurelia-framework';

@inject(SomeService)
export class App {
  constructor(someService) {
    this.someService = someService;
  }
}
```

Now the consuming class can hold on to that reference in a variable and use it:

```javascript
import {SomeService} from '../path/to/someService';
import {inject} from 'aurelia-framework';

@inject(SomeService)
export class App {
  constructor(someService) {
    this.taskName = '';
    this.someService = someService;
  }
  startTask() {
    let taskNum = this.someService.startTask(this.taskName);
    ...
  }
}
```

Most common place to use DI in Aurelia apps is view models, typically will be injecting services from client services/repository layer.

### Declaratively Registering Lifetime

Can indicate on class what instance lifetime it should have in container:

__Singleton:__ Constructed on first time its injected. All subsequent injections will get a reference to the same object that was originally created by the container. _This is the default_.

```javascript
import {singleton} from 'aurelia-framework';

@singleton()
export class SomeService {
  startTask(taskName) {
    ...
  }
}  
```

__Transient:__  A new instance of that type will be created for each injection.

```javascript
import {transient} from 'aurelia-framework';

@transient()
export class SomeModelObj {
  validate() {
    ...
  }
}  
```

### Registering Explicitly with Container

Another option for controlling lifetimes with container. Options:

__instance:__ Manually create an instance using `new`. Can set properties, call methods etc, then register this instance with container by name. Then other places that have dependency on this object will do so by name.

```javascript
import {MyService} from 'path/to/my-service';

export function configure(aurelia) {
  let myService = new MyService();
  myService.data.push('1');
  myService.data.push('2');
  myService.data.push('3');
  aurelia.use.instance('MyAwesomeService', myService);
}
```

Then other classes that depend on this would inject it by name:

```javascript
@inject('MyAwesomeService')
export class MyViewModel {
  constructor(myAwesomeService) {
    this.service = myAwesomeService;
  }
  activate() {
    ...
  }
}
```

__singleton:__ Do this in the main configure method on startup.

```javascript
import {MyService} from 'path/to/my-service';

export function configure(aurelia) {
  aurelia.use.singleton(MyService);
  ...
}
```

The `use` property on the `aurelia` object is a framework configuration object that exposes registration methods for the container.

__transient:__ Same technique as singleton.

### Injecting Resolvers

A _resolver_ is like a factory. It creates an instance of an object when the container needs it. But resolver provides an opportunity to address other types of construction patterns.

A resolver can be injected instead of a construction type. It can accept one of several strategies for construction.

__Lazy.of(T)__ Provides a function that produces a T instance when called. This is lazy loading because the type can be injected but not constructed until its actually needed.

__All.of(T)__ Provides an array of all. Useful for plugin scenarios. Multiple different types can be registered with the container, each associated with a name. All.of(T) where T is the name, will provide an instance of each type that is associated with that name.

__Optional.of(T)__ Only provides reference if it has already been constructed and exists within the container. Otherwise, returns null. Useful in notification scenarios where you don't want to hold a strong reference to an object just to be notified if something happened with it.

#### All.of example for Plugins

Suppose app allows plugins that support a certain API to be dropped into application. Want to create an instance of all the plugins that are registered with the system and call them in turn. Suppose all plugins implement a `doPluginStuff` method.

First need to register plugins with the container as transients in main configuration. Notice both plugins are registered under the same name 'SuperPlugin'.

```javascript
import {Plugin1} from './plugin1';
import {Plugin2} from './plugin2';

export function configure(aurelia) {
  aurelia.use.transient('SuperPlugin', Plugin1);
  aurelia.use.transient('SuperPlugin', Plugin2);
  ...
}
```

Now in consuming code, eg: a view model that wants to use the plugin(s), import 'All', and inject all SuperPlugin.
This will inject an _array_ of all the types that were registered under the name "SuperPlugin".

```javascript
import {inject, All} from 'aurelia-framework';

@inject(All.of('SuperPlugin'))
export class Foo {
  constructor(plugins) {
    // use the plugins however it makes sense for the scenario...
    plugins.forEach((plugin) => {
      plugin.doPluginInStuff();
    })
  }
}
```

### Globally Registering Dependencies

To avoid repetitive import declarations (eg: logger, primary data service that would be used almost everywhere). These deps can be registered globally in main app configure method. Then module is available for injection without import, and can be used in views without `<require>` element.

```javascript
export function configure(aurelia) {
  aurelia.use.globalResources('./services/myDataRepository ')
}
```

## Aurelia Routing Fundamentals

Client side mechanism to swap out views. Each view should have a unique address (url) so it can be bookmarked. Should also support browser back and forward button. Parameters also need to be supported.

Should also support hierarchical navigation:

![Hierarchical Navigation](images/hierarchical-navigation.png "Hierarchical Navigation")

Aurelia router is very flexible, and allows you to set any view as a routing container.

### Routing in Aurelia

Three basic steps to make routing work:

1. Define container for routing view with `<router-view>` element in a view template (because routing is about swapping out views within some container, when some event occurs).

1. Implement `configureRouter()` in view model associated with the view that has `<router-view>`. This is a _convention_ based method that the  router calls when it detects a url with client side addressing. This method is responsible for defining all the client side routes that the router will try to match against.

1. Define modules (view and view model pairs) for all the routes.

__Route Configuration__

| Route property  | Description  |
|---|---|
| route*  | Relative path from base URL to activate the route |
| moduleId* | Name of the module (view/view-model pair) to load into the router-view element when route is activated  |
| title  | Will be shown in the title bar or tab of browser  |
| name  | Used to identify the route for route related APIs  |
| nav  | Boolean to indicate if should include route in navigation collection for data binding  |
| href  | Override relative path URL  |

`configureRouter` method takes two arguments, a `config` object, and a reference to the `router` itself.

```javascript
export class Shell {
  configureRouter(config, router) {

    // Hold a reference to the router to support dynamically generating navigation links.
    this.router = router;

    // Set title on configuration to populate title in the browser
    config.title = 'Community User Group';

    // Define the routes using the map method of the config object.
    // map method takes a single argument that is an array of route definitions.
    // Setting an empty string makes it the default route.
    // moduleId points to the js file that contains a view model.
    // route property can be an array of addresses to match (http://localhost:2112 or http://localhost:2112/#/events)
    config.map([
      {route: ['', 'events'], moduleId: 'events'}
    ]);  
  }
}
```

Note that Aurelia locates modules based on paths relative to root of application.

__Dynamic Navigation__

Given that shell.js view model has a reference to the router:

```javascript
export class Shell {
  configureRouter(config, router) {
    this.router = router;
    ...
  }
}
```

Then shell.html view can iterate over each route to dynamically build the navigation:

```html
<ul class="nav navbar-nav">
  <li repeat.for="route of router.navigation" class="${route.isActive ? 'active' : ''}">
    <a href.bind="route.href">${route.title}</a>
  </li>
</ul>
```

### Routing Parameters

__URL Parameters__ or __Query String Parameters__. Both are passed to the `activate(params)` method in the `params` object.

To indicate a url parameter in route definition:

```javascript
{route: 'eventDetail/:eventId', moduleId: 'events/event-detail'}
```

#### Consuming Routes in View Models

__View model activate method__

Full signature is `activate(params, routeConfig, navigationInstruction)`. This is a convention based method that router will call if present in the view model, and the view model is being placed into a router view.

`params` is url and query string parameters that came out of the routing.

`routeConfig` is route definition that has arrived at this view model.

`navigationInstruction` wraps all information about current navigation step that is processing. This object has a `router` property that can be used to access the router.

__Generate URLs__

It's also possible to generate URL's using `router.generate(routeName, {params})`. This uses the `name` property of the route configuration, and `params` can have as many properties as needed, which will be passed to the `activate(params)` method of the view model associated with the named route.

__Programmatic navigation__

`router.navigate(relativePath)` Takes fully formed relative path that corresponds to a client side route.

`router.navigateToRoute(routeName, {params}, options)` Similar to `router.generate`, but has third `options` argument, with one property `useAppRouter` which specifies whether this navigation should happen on the specific router method is called on, or on the root application router. Where "root router" is the one at the top of the view hierarchy.

### Generating Route URLs from Routing Configuration

Avoid "hard-coding" path information in urls in templates, for example, given route configuration:

```javascript
{route: 'eventDetail/:eventId', moduleId: 'events/event-detail', name: 'eventDetail'}
```

And template:

```html
<a href="#/eventDetail/${event.id}">${event.title}</a>
```

This is brittle, if route changes, also need to change template. A better approach is to generate the url programmatically:

```html
<a href.bind="event.detailUrl">${event.title}</a>
```

Where `event.detailUrl` is populated in the parent view model (events.js):

```javascript
this.events.forEach((item) => {
  item.detailUrl = this.router.generate('eventDetail', {eventId: item.id});
});
```

### Using Query String Parameters

For example, to support filtering a displayed list by some criteria. [Example](community-app-routing/src/events/events.js)

### Programmatic Controlling Navigation

Example, in template:

```html
<button click.trigger="navigateEvents()" class="btn btn-default">Back to Events</button>
```

And view model:

```javascript
navigateEvents() {
  this.router.navigateToRoute('events');
}
```

## Routing Beyond the Basics

### Screen Activation Lifecycle

![Screen Activation Lifecycle](images/screen-activation-cycle.png "Screen Activation Lifecycle")

`canActivate` is called before `activate`.

When another view is about to be loaded, router will call `canDeactivate` on the current view.

`deactivate` final notification to view model that its about to become inactive, do cleanup here like removing event handlers

__Sequence__

1. Navigation instruction comes in to the router
1. Router calls `canDeactivate` on current view model. If this method returns false, or a promise that resolves with false, then navigation is cancelled. Otherwise...
1. Router calls `constructor` on destination view model, if it hasn't already been constructed in a previous navigation cycle. By default, Aurelia keeps all view models that have been constructed so they're only ever constructed once. It's possible to override this behaviour (later in course).
1. Router calls `canActivate` on destination view model. If this method returns false or a promise that resolves to false, navigation is prevented. Otherwise...
1. Router calls `deactivate` on current view model.
1. Router calls `activate` on destination view model.

[Example rejecting navigation into](community-app-routing/src/jobs/jobs.js)

[Example rejecting navigation away from](community-app-routing/src/discussion/discussion.js)

### Sibling Navigation Panes with ViewPorts

![Navigation View Ports](images/navigation-viewports.png "Navigation View Ports")

Aurelia router supports multiple router-view's within a single containing view. With multiple router-view's in the same container, they must be named to distinguish.
Route definition will include what module goes in each named _view port_.

`viewPorts` section of route definition will have one entry for each named router-view that is in corresponding view.

```javascript
config.map([
  {
    route: ['', 'events'],
    viewPorts: {
      mainContent: {moduleId: 'events/events'},
      sideBar: {moduleId: 'sidebar/sponsors'}
    },
    name: 'events',
    title: 'Events',
    nav: true
  }
])
```

[Example](community-app-routing/src/shell.js)

### Child (Nested) Routers

![Child Routers](images/child-routers.png "Child Routers")

Sample usage, a tabbed view, for example, given a view that displays a list of community events, have a separate tab for past events vs future events.

Child routes are expected to be relative to parent route. Therefore must use href property on children, [see example](community-app-routing/src/events/events.js).

### Activation Strategy

Used by router to determine what to do when multiple routes have the same module, i.e. what kind of lifecycle should be applied to that instance of the module.

To set this, in the view models that are going to be plugged into a given router view, implement the `determineActivationStrategy` method. This method is called by the framework by convention, and should return an activation strategy enumerated value. Possible values are:

* `.no-change` (default), reuse instance with no lifecycle events, i.e. no lifecycle methods will be fired as route changes. For most cases, this is not the desired outcome when multiple routes use the same module, i.e. you probably want something different to happen.
* `.invokeLifecycle` call lifecycle methods on the ViewModel instance each time the route switches, canActivate, activate, etc. However, the same instance will be reused for all routes its a part of. So if instance is maintaing some data like `this.listOfThings`, need to ensure that `activate` method replaces that collection so the view will be refreshed.
* `.replace` construct new instance of ViewModel and invoke full lifecycle on it.

### Push State

This is a term from HTML5 spec, to get rid of the "#" in client side relative addresses in browser. Why do this?

* SEO - some web crawlers ignore all content to right of hash
* Friendlier urls

To implement it:

* (route)config.options.pushState = true in configureRouter (top level router only?), in this case, browser will be able to partition url from base and treat everything as a relative address.

```javascript
configureRouter(config, router) {
  config.options.pushState = true;
  ...
}
```

* ensure config.js has baseURL set (usually "/")

```javascript
System.config({
  baseURL: "/",
  ...
})
```
* setup server handling to ignore client route portions of URL for SPA base route (implementation details vary by server/framework)
* if any routes use the `href` override, remove the "#" from those.

Note a consequence of this is can no longer access `http://localhost:2112/index.html` because `index.html` will be treated as a client side path, which does not exist.

Another consequence, this only works if start from root `http://localhost:2112`. But if user first lands on a relative path, for example `http://localhost:2112/discussion`, get error "Cannot GET /discussion". Because on a full refresh, this goes to the server, but "/discussion" only exists client side. Need to configure server such that any request for client root address goes to index.html (or whatever the root page for SPA is).

### Navigation Pipeline

A pipeline software architecture supports chaining multiple components together and plug additional components in as middleware, where there is a series of executions, such that custom steps can be inserted into the process.

Aurelia uses a pipeline architecture for router.

### Custom Navigation Pipeline Steps

Can plug in custom processing steps to be run during the routing process, via middleware (or interception). To do this:

* Define a class with a `run` method

```javascript
run(navigationInstruction, next) {
  ...
  return next();
}
```

`navigationInstruction` is an object with information about the current navigation.

`next` is a function that when called, causes the next thing in the pipeline to execute.

`next` returns a promise so you can chain handling on the end of that.

* Add the custom step to the route configuration object:

```javascript
config.addPipelineStep('authorize', MyCustomPipelineStep);  
```

First argument is name of an available pipeline, second argument is name class containing your custom step.

Aurelia wires it up and will call your custom `run` method after the normal authorize pipeline processing.

* Can perform custom processing steps before invoking next function, which triggers screen activation lifecycle for destination view.

* Two pipelines are available to customize:
    * authorize (beginning)
    * modelbind (end)

[Example custom pipeline step](community-app-routing/src/shell.js)

## Data Binding Fundamentals

### Overview

Data binding is about replacing custom push/pull dom logic with declarative syntax that sets up relationship between elements' attribute value and the property value on some data object in a view model. Enables loose coupling. Reduces code effort and maintenance.  

Bindings are loosely coupled bonds between View and ViewModel, this is a key enabler for MVVM.

Aurelia has an _adaptive data binding_ system. This is to optimize performance, avoids checking for changes too often.
Easy to use and simple syntax.

### Adaptive Data Binding System

When doing data binding, bindings in the DOM need to know when the properties they're bound to (could be view model properties or model properties exposed through view model) have changed.

Data needs to flow in both directions depending on type of binding.

Need notifications from property in view model to attribute value that is bound in the view.

Also need flow/notification when user enters new value in input elements, and that element is bound to a view model property.

Aurelia also supports binding between elements, attribute value on one element can drive attribute value on another element.

Aurelia supports:

* 2-way data binding (flowing in both directions)
* 1-way data binding (flows from view model properties to view elements)
* 1-time - data flows once from view model to view on initial rendering, and then the relationship is removed

Aurelia picks the best strategy based on element type and attribute.

__How does Adaptive Binding work__

To make sure that properties being observed cause the screen to refresh when values change...

* DOM elements: Built in adapters per input element type, that know for a given element type what the attributes/properties on that element are that contain the values that will change as user inputs new data. The adapter triggers on those changes and causes the data to flow to the view model based on 2-way data binding.
* When the templating mechanisms are hooking up a binding, they look for _ObserverLocators_, which is a built in abstraction in Aurelia. Observe certain triggers when to trigger flowing data through a binding. If there is an ObserverLocator associated with a property, Aurelia's binding system can monitor that instead of the property itself.
* If there is no ObserverLocator and its a plain old JavaScript property, Aurelia rewrites it with `object.defineProperty` to wire in its own observer locator in `set` block.
* Computed properties (i.e. getter function in view model), this is a function so Aurelia can't know what's inside the body of that function that will produce a value. You can "hint" using `@computedFrom`, which specifies what other properties in that class the computed value being generated from. Aurelia then wires up ObserverLocators on the dependent properties, and whenever one of those changes, it can assume the computed property has changed and will get the new value, and update the DOM with the bound value.
* Fall back mechanism: dirty checking, watches all the properties by polling and when value changes, it refreshes the bindings based on that.
* Arrays: For data binding to arrays, Aurelia needs to know when items are added and removed from array.
    * Rewrites array mutation methods `push`, `pop`, `slice`, `shift`, to first notify bindings, then do the array mutation
    * Its not possible to intercept indexed assignment or mutation by libs such as lodash or underscore
* Can signal binding behavior to trigger a refresh manually, this is a built-in _binding behaviour_ (discussed later in course).


### Primary Data Binding Syntax

The following techniques will be good to cover around 90% of use cases:

*  On any property of any html element, add ".bind" on that property name as an attribute, and the expression pointed to will be used to resolve against a view model property, or a model property exposed by the view model, to obtain the value for that property on the element through data binding.

  ```html
  <element property.bind="viewMOdelProperty" ...></element>
  ```

* String interpolation (or string template). Binding expression is placed inside ${...}. It's a path to a property value in the view model (or model object exposed by view model). This produces a value that will be placed into the position where the string interpolation binding is in the template.
For example, populate product description in the span:

  ```html
  <span>${product.description}</span>
  ```

  In this example, the value of `filename` will be concatenated into the string that forms the `src` attribute of the `img` tag:

  ```html
  <img src="images/uploads/${filename}" alt="something" />
   ```

* The `for` command on the `repeat` binding is used to loop over a collection. `repeat.for` is placed on any html element, and for each item in the collection, an instance of that element will be rendered (`<tr>` in example below).

  As it renders each element, a local binding context is created that is the looping variable (`customer` in the example below). Inside the loop, this object's properties are available.

  ```html
  <table>
    <tr repeeat.for="customer of customers">
      <td>${customer.firstName}</td>
      <td>${customer.lastName}</td>
      <td>${customer.phone}</td>
    </tr>
  </table>
  ```

### Binding with innerHTML and textContent

"textContent" is an html attribute on a content element, and it lets you set the content to any plain text. So we can use `textContent.bind` as follows:

```html
<h3 textContent.bind="event.title"></h3>
```

To set the inner html of an element, use `innerHtml.bind`. In this case, the value `event.description` can include markup and it will be rendered:

```html
<td innerHtml.bind="event.description"></td>
```

__BEWARE XSS!__ This is vulnerable to cross site scripting. If the text to be rendered as `innerHtml` comes directly from user input without being sanitized, user could enter malicious code in script blocks or event handlers that would get executed when that content is rendered. To prevent this, MUST sanitize the markup while rendering.

Aurelia comes with a `sanitizeHTML` value converter, which can be inserted into a binding statement using the pipe symbol:

```html
<p innerHtml.bind="event.description | sanitizeHTML"></p>
```

Data flows through value converter when moving between the view model and the target property on the element in the markup.
Value converter transforms the data.

`sanitizeHTML` only removes script blocks but that's not good enough to prevent XSS because scripts can also be executed through event handlers attached to elements in the text, and this kind of code is not removed. For better sanitization, use [sanitize-html](https://www.npmjs.com/package/sanitize-html)

### Data Binding Modes: Controlling Data Flow

If you don't want Aurelia's default behaviour for ".bind", specify either `.two-way`, `.one-way` or `.one-time`.

Example:

```html
<div textContent.two-way="event.description" contenteditable="true"></div>
```

Note that using the default `.bind` will have same effect because Aurelia detects the "contenteditable" attribute and determines that this binding should be two way.

### Event Bindings Overview

In addition to flowing data, binding can also be used to invoke methods on view model.

__event.delegate:__ Can take an event on an element, add ".delegate" and point it at a method on view model.
This will wire up an event handler at the document level once in the html dom for that particular method. This is most efficient.
So if there are multiple elements on the page that are wired up to the same event and the same handling method, there will only be one event handler in memory invoking the target method when the event occurs. It does this by allowing the event to bubble up in the DOM.

__event.trigger:__ Wire up event handler on element itself. Wires up an event every time its used, consumes more memory than delegation.

__attribute.call:__ Passes function pointer to attribute for later invocation. This is only meaningful for custom elements and attributes. It doesn't invoke the method.

__$event:__ Can pass event argument to target method of event.delegate or event.trigger.

```html
<button type="button" click.trigger="doSomething()">Click Me</button>
```

```javascript
doSomething() {

}
```

## Data Binding Beyond the Basics

### Behind the Curtain of Data Binding

Data binding in Aurelia is closely tied to the templating system, which is responsible for taking chunks of DOM defined as views inside a template element, and to bring them into the DOM and render them, based on dynamic data based on data binding.

Aurelia goes through a template parsing process, and extracts from the data bindings, an object model based on _BindingExpression_.
Binding expressions are aware of the source object and property that the values come from and the target dom element and what property or attribute on that element is being driven by the data binding.

The parsing process forms an AST, setting up sequence of instructions that need to be executed in order to populate dom dynamically based on things its data bound to. The AST is executed through the _TaskQueue_, which supports executing bundles of asynchronous work efficiently.

### Specialized Bindings

`if`, `show`, `ref` are custom attributes that come with Aurelia.

__if.bind__ Adds or removes an element from DOM based on bound value. If the value is true, part of DOM, if the value is false, will not be part of DOM.

__show.bind__ Hides/shows an element from the DOM based on the bound value. Still part of DOM, but will be hidden if bound value is false.

__ref__ Lets you add a name to an element that can be used as a data binding source reference. Can bind one element's property to a property on another element. Value of one of elements properties can be used to drive the behaviour and presentation of some other element in the DOM.

### Advanced repeat.for Capabilities

Can bind to Map collections:

```html
<div repeat.for="[key,value] of mapCollection">
  ${key} : ${value}
</div>
```

Where view model would have something like:

```javascript
this.mapCollection = new window.Map();
this.mapCollection.set('a', 'Alpha');
this.mapCollection.set('b', 'Beta');
this.mapCollection.set('c', 'Charlie');
this.mapCollection.set('d', 'Delta');
```

Loop context values inside body of loop in markup:

`$index` 0-based index in loop.

`$first`, `$last` boolean properties set to true if on first or last item respectively.

`$even`, `$odd` boolean properties set to true if current index is even or odd respectively.

`$parent` reference to binding context that exists just outside of repeat.for. Recall repeat.for sets up a local binding context inside that loop. But if you want access to properties or methods exposed by the containing view model, use $parent.

You can also push `$parent` into the `<compose>` element:

```html
<div repeat.for="event of events">
  <compose model.bind="$parent" view="events/event.html"></compose>
</div>
```

### Style / CSS Binding

__style.bind (string)__ Bind to a string, for example `<div style.bind="styleString"></div>`,
where `styleString` is exposed by view model must follow format of any style value you would use in html.

__style.bind (object)__ Bind to object, for example `<div style.bind="styleObjet"></div>`,
where `styleObject` is exposed in view model, and its keys correspond to css properties, and the values are the values set against those.

__style/css interpolation bindings__  Note that `style` string interpolation does not work in IE, but `css` does, so use this format:

```html
<div css="background: ${customerColor}">...</div>
```

### Working with <select>

This is a special case of data binding, because `<select>` element has two data bound contexts:

* Collection for options (need to iterate over).
* Selected option (could be single or multi-selection), need to set appropriate value on view model based on the selection.

Select element can be bound to a collection of strings or a collection of objects.

__Binding to strings:__

```html
<select id="state" value.bind="job.location.state">
  <option repeat.for="state of states" value.bind="state">
    ${state}
  </option>
</select>
```

`repeat.for` allows binding to multiple option elements based on the collection.

On each option element, specify what is the value associated with this option. Values on options can only be strings.

Then need to specify what happens when selection is made, in this case `job.location.state` is bound on select element to drive what property on view model will be set when a selection is made.

If this is a single selection, `job.location.state` should be a string.
But for multi-select mode, it should be an array, and the `value.bind` on `<select>` element will add and remove items from this array as selections are made.

__Binding to objects:__

```html
<select id="state" value.bind="job.location.state">
  <option repeat.for="state of states" model.bind="state.abbreviation">
    ${state.name}
  </option>
</select>
```

For using a collection of objects, use `model.bind` instead of `value.bind` on the `<option>` element.
This provides access to the objects properties via dot notation.

### Checkboxes and Radio Buttons

Similar to `<select>`, need to define where value is coming from that will be set when checkbox is checked.

__Value source: value.bind for strings__

For strings, use `value.bind` on `<input>` element, just like for text type inputs.

```html
<label repeat.for="jobSkill of jobSkills">
  <input type="checkbox" value.bind="jobSkill" checked.bind="$parent.job.jobSkills ">
  ${jobSkill}
</label>
```

If the value associated with checkbox is an object, use `model.bind` instead of `value.bind`.

`checked.bind` identifies what's going to be set. This points to property on view model.

__Radio Buttons__

Same behaviour as checkboxes.

name set to associate radio buttons.

### Binding with Value Converters

Value converters allow you to put an object in the data binding system that sits logically in between the source and target element.

And it converts the values that are passing back and forth (in a bi-directional manner if doing two way data binding),
can convert the value as it goes _from_ the view model property _to_ the view element.

And it can convert the value coming back in from some input element in the view, to the view model property.

To declare a value converter, follow this naming convention:

```javascript
export class MyValueConverter {
  toView(value) {
    // given a value in view model, apply some custom logic
    // and return a value that will be displayed in the view.
  }
  fromView(value) {
    // only need this for two-way data binding.
    // given a value from element in DOM, apply some custom logic
    // and return a value that will be set in the view model.
  }
}
```

Usage in code strips off the `ValueConverter` portion of the name. Use the pipe symbol in template:

```html
<td>${ job.needDate | dateFormat }</td>
```

Can also pass parameters to value converter. For example, to pass in a date format string and localization parameters,
use a colon separated list of parameter values:

```html
<td>${ job.needDate | dateFormat: 'MM/YYYY':'en-UK' }</td>
```

### Extending Data Binding with Binding Behaviours

Extensibility hook into the binding system (advanced topic).

Binding behaviour is an object you can insert into the binding process, that has full access to the binding itself, the binding scope,
the lifecycle events of a binding expression (updateSource, updateTarget, callSource). And you can intercept these to change the normal
binding behaviour.

Binding behaviours get attached with `&` symbol. They can be combined with ValueConverters.
This is a pipline model, where a value that's passing through a binding, could be passed through 0-many value converters,
as well as 0-many binding behaviours. They can be chained together in the binding expression.

Binding behaviours can also take parameters (`:` separated).

#### Built-in Binding Behaviours

__updateTrigger__ Override _when_ data flows through a data binding.

By default, any input or change event on an element causes the data binding to push the current value down into the source object.
i.e. when user types into a text box, data flows every time something changes within that element.

But that may be too soon in some cases, for example, may want to first validate the input from user.

updateTrigger allows you to define other events that should be watched that will be the triggers for pushing the value to the source object.

Example: Wait till input field loses focus to update the value:

```html
<input type="text" value.bind="job.needDate | dateFormat: 'MM/YYYY' & updateTrigger: 'blur'">
```

__throttle__ Delays when updates happen. Useful if you have a high rate of change data feed like stock market or telemetry data flowing in from an instrument. Then don't want to re-render the screen every moment that something changes. Throttle behaviour takes a delay in ms (default 200), and will delay the update by that time period.

Throttle can also be used to delay flow of data from input element to view model (eg: a really fast typer).

Example: Throttle refreshes on a live data feed (simulated here with setInterval):

```javascript
export class Shell {
  constructor() {
    // demo throttle binding behavior
    setInterval(() => this.timeIs = moment().format('hh:mm:ss.SSS'), 100);
  }
  ...
}
```

```html
<div>${timeIs & throttle:1500}</div>
```

__debounce__ Waits to update until a time period has passed with no changes (default 200ms).
For example, a search input that makes an http call, don't want to do this on every keystroke, so set a debounce to wait until user has stopped typing for some time.

__signal__ Triggers a binding update from anywhere in the app.
* Specify signal name as behaviour parameter
* Inject BindingSignaler and call signal with name

Useful when changes happen in the app that the binding is not aware of.
eg: login session for user timed out, and screen needs to update based on user being no longer logged in.
In this case, raise a signal. Inject an object `BindingSignaler` and call `signal` method on it passing a name.
On behaviour, specify that same signal name as a parameter.
Then when the `BindingSignaler` signals with that name, it will trigger any bindings that have that binding behaviour, with that signal name,
to go ahead and get their values again from the source object.

Example: Left off 6:23 "Using the Signal Binding Behavior"

__Binding mode behaviours__ For use with interpolation bindings, which otherwise default to one way,
and there's no built in syntax to express a different mode for the binding.

* oneWay
* oneTime
* two Way

Example:

```html
<template>
  ${person.fullName & twoWay}
</template>
```

#### Custom Binding Behaviours

Easy to create, _BUT_ requires deep understanding of the binding engine. Looks like:

```javascript
export class MyCustomBindingBehavior {
  bind(binding, scope) {
    // called when first parsing a binding expression in the markup
    // "binding" is the binding expression, including source object, target element, bound properties, value converters, etc
    // "scope" is binding scope within the view, what element you're on, what elements surround it
  }
  unbind(binding, scope) {
    // unbind phase is when view is being torn down (navigating away)
  }
}
```

Used like built-in behaviours, `&` on binding.

## Extending Aurelia with Custom Elements and Attributes

Extensibility points to encapsulate custom logic and put it in a form that's easily reusable in multiple views and applications.

### Custom Elements

Define user interface and supporting logic that drives some functionality in application. Wrapped in a single tag in markup.

To define a custom element, just need a view and view model pair. Any view and view model can be used as a custom element.

Can also use a decorator to explicitly declare that a class is designed to be a custom element, and specify the name of the tag to be used in the markup.

To use a custom element, use the `<require>` tag to pull in that module into the containing view.

Custom elements can expose data bindable properties. These properties are available in the markup and can be set statically or set dynamically through data binding.

Custom elements can handle lifecycle events (stages element goes through), from when it gets created, to when it gets data bound, to when it gets inserted in the dom, when it gets pulled out of the dom, and when its unbound. Can define methods in element class definition to handle these events, and Aurelia will call those methods, passing.

Not every aspect of what's shown in the view of custom element needs to be supplied by the custom element itself. Can put placeholders or templates into the definition of the custom element and allow the user of the element to supply the fragments of markup that will be placed into those placeholders.

__IMPORTANT:__ As Aurelia creates each custom element (read it in through the markup, create an instance of the view and view model, and place into the DOM), Aurelia wants to provide good isolation and encapsulation for that element. Therefore Aurelia will create a separate child container instance, that becomes a child of the parent views container, so that instances of the element can be placed in that container and resolve into child elements without interfering with anything else in the surrounding views' dom.

### Turning a View and ViewModel into a Custom Element

[Example View ](community-app-routing/src/common/nav-bar.html) | [Example View Model](community-app-routing/src/common/nav-bar.js)

Any view and view model pair can form a custom element. In the above exmaple, the navigation is pulled out of the shell and into a custom element.

Note naming conventions, if view model has `export class NavBar...`, then it will be used in a template like `<nav-bar></nav-bar>`.

To make the router items appear in the nav bar custom element, use `@bindable` as a class property in the custom element view model:

```javascript
// nav-bar.js
import {bindable} from 'aurelia-framework';

export class NavBar {
  @bindable router;
}
```

This makes it bindable in the custom element's template, and ALSO when its being used as a custom element in the containing view:

```html
<template>
  <!-- shell.html -->
  <require from="common/nav-bar"></require>

  <nav-bar router.bind="router"></nav-bar>
</template>
```

This works because the shell view model exposes the router:

```javascript
// shell.js
export class Shell {
  constructor() { }

  configureRouter(config, router) {
    this.router = router;
    ...
  }

  ...
}  
```

### Custom Elements Lifecycle

There are a number of methods you can declare on a custom element's view model that will be invoked by the framework when its being injected into the DOM. They are called in this order:

__created(view)__ Called when the custom element is constructed. This method is passed a reference to the containing view that this custom element is being rendered into.
This provides access to the view's DOM and can be manipulated here. This is useful if the custom element needs some context about where its contained and going to do some adaptive rendering.

__bind(bindingContext, overrideContext)__ Gets called as the element is being data bound. Gets passed a `bindingContext`,
which is the parent view model. `overrideContext` is used by Aurelia and generally you don't need to touch this.

__attached()__ Called when this custom element is attached to a parent DOM element.

__detached()__ Called when this custom element is pulled out of the DOM.

__unbind()__ Called when this custom element is pulled out of the DOM.

### Controlling the Name and Container of a Custom Element

There are options for determining what name of custom element will be when its used in markup.

By convention, its the class name, lower-cased, with dashes between capitals. Eg: `NavBar` and `<nav-bar>`.

You can name class `NavBarCustomElement`, to explicitly tell Aurelia that this is a custom element,
it will strip off the `CustomElement` portion of the name and remain with `NavBar`.

If you want the custom element named something different than class, use the `customElement` decorator, passing in name to be used in markup, for example:

```javascript
import {customElement} from 'aurelia-framework';

@customElement('navigation-bar')
export class NavBar {
  ...
}
```

```html
<navigation-bar></navigation-bar>
```

Note that regardless of naming, Aurelia will create that element in the DOM, even though there is no such element in HTML5.
If you don't want this to show in the dom, use the `containerless` decorator.

### Implementing Replacable Parts in a Custom Element

For example, the navbar-brand is currently hard-coded in the nav-bar.html as `<a class="navbar-brand" href="#">Community Board</a>`, which limits re-use.

It would be better if custom element allows user to plug in their own content somewhere in the markup,
to achieve this, use a template element with "replaceable" attribute, and give it a name using "part" attribute.

```html
<!-- nav-bar.html (custom element markup) -->
<template>
  <nav class="navbar">
    ...
    <a class="navbar-brand" href="#"><template replaceable part="brand"></template></a>
    ...
  </nav>
</template>
```

To supply the content that nav-bar is now expecting, go to the view that's using it (shell.html in this example),
and provide a template inside the custom element usage, whose name matches the "part" value specified in custom element definition:

```html
<!-- shell.html (view using nav-bar custom el) -->
<template>
  ...
  <nav-bar router.bind="router">
    <template replace-part="brand">
      <span>Acme Corp.</span>
    </template>
  </nav-bar>
  ...
</template>
```

### Custom Attributes

[Example](community-app-routing/src/common/speaker-image.js)

* Even more powerful than custom elements. Can customize/extend functionality of existing elements.
* Can listen to element events and/or check values of other attributes on the element to decide when/what to do.
* Also have access to surrounding DOM for the element.

Custom attribute can drive behaviour in a few different ways:

* Just by being attached to an element, with no value: `<some-element do-back-flips />`
* Based on single value set on attribute (more common usage): `<some-element do-back-flips how-many="5" />`,
value can be set statically or dynamically through a binding expression,
in this case, "do-back-flips" and "how-many" are two separate attributes that interact.
* Based on multiple named values using _options_ binding `<some-element backflips="enabled.bind: areBackflipsEnabled; howMany: 5" />`,
in this case, there's a single attribute named "backflips", with two properties "enabled" and "howMany",
where "enabled" is set dynamically through data binding to a property on the parent view model, and "howMany" is set staticaly.
* Custom attributes can clone a fragment from the DOM and render it out conditionally, event-driven, or multiple times,
(eg: Aurelia "if" and "repeat" attributes).

For naming/usage of custom attributes, can follow a naming convention on the view model class, for example `SpeakerImageCustomAttribute`, or use `@customAttribute(speaker-img)`.

With any view model, you can define methods for any property, that will be called by the framework when that property is changed, especially through data binding. The name of this method will be "propertyName" + "Changed". For a custom attribute, this is "valueChanged".

#### Template Controller

[Simplified if](community-app-routing/src/common/my-if.js) | [Example from scratch](community-app-routing/src/common/items-control-2.js)

An attribute that is capable of taking a chunk of markup from html and reproducing that either one time or multiple times, driven by behaviour of attribute.

"if" binding built into Aurelia is an example of this.

Use `@templateController` decorator on attribute class view model.

Inject `BoundViewFactory`, which is used to produce an _instance_ of chunk of markup that custom attribute is on.

Inject `ViewSlot`, which is place in DOM that declaration was, that you can use as the place where you're going to produce one or more instances of that chunk of markup.
