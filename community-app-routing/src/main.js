import 'bootstrap';
import {DataCache} from 'data-cache';
import {Plugin1} from 'plugin1';
import {Plugin2} from 'plugin2';

export function configure(aurelia) {
  let dataCache = new DataCache();
  dataCache.data.push('a');
  dataCache.data.push('b');
  dataCache.data.push('c');
  aurelia.use.instance('TheBestCacheEver', dataCache);

  // aurelia.use.singleton(DataCache);
  // aurelia.use.transient(DataCache);

  aurelia.use.transient('SuperPlugin', Plugin1);
  aurelia.use.transient('SuperPlugin', Plugin2);

  aurelia.use.standardConfiguration().developmentLogging();
  aurelia.start().then( a => a.setRoot('shell'));
}
