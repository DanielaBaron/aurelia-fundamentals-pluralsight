import {eventsData} from './events-data';
import moment from 'moment';

function filterAndFormat(pastOrFuture, events) {
  // clone events so we're not modifying raw events stored in data repository
  let results = JSON.parse(JSON.stringify(events));

  if (pastOrFuture == 'past') {
    results = results.filter(item => moment(item.dateTime) < moment());
  } else if (pastOrFuture == 'future') {
    results = results.filter(item => moment(item.dateTime) > moment());
  } else {
    results = results;
  }

  results.forEach(item => {
    let dateTime = moment(item.dateTime).format('MM/DD/YYYY HH:mm');
    item.dateTime = dateTime;
  });

  return results;
}

export class DataRepository {
  constructor() {
    this.eventsData = eventsData;
  }

  getEvents(pastOrFuture) {
    return new Promise((resolve, reject) => {
      this.events = this._sortByDate(this.eventsData);
      resolve(filterAndFormat(pastOrFuture, this.events));
    });
    // return new Promise((resolve, reject) => {
    //   if (!this.events) {
    //     setTimeout(() => {
    //       this.events = this.eventsData;
    //       this.events.forEach(item => {
    //         let formattedDate = moment(item.dateTime).format('MM/DD/YYYY HH:mm');
    //         item.dateTime = formattedDate;
    //       });
    //       resolve(this.events);
    //     }, 2000);
    //   } else {
    //     resolve(this.events);
    //   }
    // });
  }

  _sortByDate(events) {
    return events.sort((a,b) => {
      return a.dateTime >= b.dateTime ? 1 : -1;
    });
  }

  getEvent(eventId) {
    return new Promise((resolve, reject) => {
      let matchingEvent = this.eventsData.find((item) => item.id == eventId)
      resolve(matchingEvent);
    });
  }
}
