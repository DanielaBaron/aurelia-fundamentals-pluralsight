export var eventsData = [
  {
    "id" : 124,
    "description" : "REST is an architectural style...",
    "dateTime" : "2016-12-26T23:30:00.000Z",
    "title" : "Designing RESTful Services",
    "speaker" : "Brian Noyes",
    "isMvp" : true,
    "image" : "BrianNoyes.jpg",
    "detailUrl" : "https://news.google.ca/"
  },
  {
    "id" : 131,
    "description" : "When modeling your <strong>data</strong> for an enterprise...",
    "dateTime" : "2013-08-27T23:30:00.000Z",
    "title" : "Entity Framework Code First",
    "speaker" : "Shahed Chowdhuri",
    "image" : "Shahed.jpg",
    "detailUrl" : "https://news.google.ca/"
  },
  {
    "id" : 149,
    "description" : "The talk discusses the up-and-coming Web Components...",
    "dateTime" : "2015-01-27T23:30:00.000Z",
    "title" : "Web Components and Polymer",
    "speaker" : "Steve Albers",
    "image" : "steve.jpg",
    "detailUrl" : "https://news.google.ca/"
  },
  {
    "id" : 152,
    "description" : "blah blah",
    "dateTime" : "2016-11-20T23:30:00.000Z",
    "title" : "Web Application Security",
    "speaker" : "Kevin Jones",
    "image" : "steve.jpg",
    "detailUrl" : "https://news.google.ca/"
  }
]
