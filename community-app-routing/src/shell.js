import toastr from 'toastr'
import moment from 'moment'

export class Shell {
  constructor() {
    // demo throttle binding behavior
    // setInterval(() => this.timeIs = moment().format('hh:mm:ss.SSS'), 100);
  }

  configureRouter(config, router) {
    this.router = router;

    // Uncomment to enable HTML5 PushState (requires server side support!)
    // config.options.pushState = true;

    // custom pipeline step
    config.addPipelineStep('authorize', ToastNavResult);
    // config.addPipelineStep('modelbind', ToastNavResult);

    config.title = 'Community User Group';

    config.map([
      {
        route: ['', 'events'],
        viewPorts: {
          mainContent: {moduleId: 'events/events'},
          sideBar: {moduleId: 'sidebar/sponsors'}
        },
        name: 'events',
        title: 'Events',
        nav: true
      },
      {
        route: 'eventDetail/:eventId',
        viewPorts: {
          mainContent: {moduleId: 'events/event-detail'},
          sideBar: {moduleId: 'sidebar/sponsors'}
        },
        name: 'eventDetail'
      },
      {
        route: 'jobs',
        viewPorts: {
          mainContent: {moduleId: 'jobs/jobs'},
          sideBar: {moduleId: 'sidebar/interest '}
        },
        name: 'jobs',
        title: 'Jobs',
        nav: true
      },
      {
        route: 'discussion',
        viewPorts: {
          mainContent: {moduleId: 'discussion/discussion'},
          sideBar: {moduleId: 'sidebar/interest '}
        },
        name: 'discussion',
        title: 'Discussion',
        nav: true
      }
    ]);
  }
}

// Customize navigation pipeline
class ToastNavResult {
  run(navigationInstruction, next) {
    // first execute the next step in the navigation pipeline
    // and then take the resulting object "a", which has status flag, which will be completed or cancelled
    return next().then(a => {
      toastr.info(a.status);
      return a;
    });
  }
}
