export class Jobs {

  constructor() {
    this.jobs = [
      {title: 'Developer', location: 'New York', skills: ['Java', 'JavaScript']},
      {title: 'Engineer', location: 'LA', skills: ['Python', 'MongoDB']}
    ];
  }

  // demonstration of screen activation lifecycle methods
  // canActivate(params, routeConfig, navigationInstruction) {
  //   // In a real app, might make a request to server to determine that we can't navigate to this view after all
  //   return new Promise((resolve, reject) => {
  //     setTimeout(_ => {
  //       resolve(false);
  //     }, 2000);
  //   });
  // }
}
