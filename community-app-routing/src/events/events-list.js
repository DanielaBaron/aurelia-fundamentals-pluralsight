import {inject} from 'aurelia-framework';
import {DataRepository} from '../services/data-repository';
import {Router, activationStrategy} from 'aurelia-router';

@inject(DataRepository, Router)
export class EventsList {
  constructor(dataRepository, router) {
    this.dataRepository = dataRepository;
    this.router = router;
    this.whoareyou="I am the Events List View Model"
  }

  determineActivationStrategy() {
    return activationStrategy.invokeLifecycle;
  }

  // routeConfig contains route definition that got us to this view, including route name property
  activate(params, routeConfig) {
    let pastOrFuture = routeConfig.name == '' ? 'future' : routeConfig.name;
    return this.dataRepository.getEvents(pastOrFuture)
      .then(events => {
        this.events = this._filteredResults(params, events);
        this.events.forEach((item) => {
          item.detailUrl = this.router.generate('eventDetail', {eventId: item.id});
        });
      });
  }

  _filteredResults(params, events) {
    let filteredResults = [];

    if (params.speaker || params.title) {
      events.forEach(item => {
        if (params.speaker && item.speaker.toLowerCase().indexOf(params.speaker) >= 0) {
          filteredResults.push(item);
        }
        if (params.title && item.title.toLowerCase().indexOf(params.title) >= 0) {
          filteredResults.push(item);
        }
      });
    } else {
      filteredResults = events;
    }

    return filteredResults;
  }

}
