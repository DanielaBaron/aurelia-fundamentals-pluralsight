export class Events {
  configureRouter(config, router) {
    this.router = router;
    config.title = 'Events';
    config.map([
      {
        route: ['', 'future'],
        name: 'future',
        moduleId: 'events/events-list',
        title: 'Future Events',
        nav: true
      },
      {
        route: 'past',
        name: 'past',
        moduleId: 'events/events-list',
        title: 'Past Events',
        nav: true,
        href: '#/events/past'
        // if push state is enabled
        // href: '/events/past'
      }
    ]);
  }
}
