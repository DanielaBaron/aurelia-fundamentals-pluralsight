import {inject} from 'aurelia-framework';
import {DataRepository} from 'services/data-repository';
import {Router} from 'aurelia-router';

@inject(DataRepository, Router)
export class EventDetail {
  constructor(dataRepository, router) {
    this.dataRepository = dataRepository;
    this.router = router;
  }

  activate(params) {
    return this.dataRepository.getEvent(params.eventId)
      .then(item => this.eventDetail = item);
  }

  navigateEvents() {
    this.router.navigateToRoute('events');
  }
}
