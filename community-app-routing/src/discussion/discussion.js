import {activationStrategy} from 'aurelia-router';

function getDiscussionInput() {
  // fake data access
  return '';
}

function cloneObject(obj) {
  // js trick to get full deep clone of object
  return JSON.parse(JSON.stringify(obj));
}

export class Discussion {
  constructor() {
    this.someUserName = 'Alice';
  }

  determineActivationStrategy() {
    return activationStrategy.invokeLifecycle;
  }

  activate() {
    this.discussionInput = getDiscussionInput();
    // so we can detect unsaved changes
    this.originalInput = cloneObject(this.discussionInput);
  }

  save() {
    // simulate save
    this.originalInput = cloneObject(this.discussionInput);
  }

  canDeactivate() {
    if (JSON.stringify(cloneObject(this.discussionInput)) != JSON.stringify(this.originalInput)) {
      if (confirm('Unsaved dta, are you sure you want to navigate away?')) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  bind(bindingContext) {
    console.log('DISCUSSION VIEW: bind is called');
  }
}
