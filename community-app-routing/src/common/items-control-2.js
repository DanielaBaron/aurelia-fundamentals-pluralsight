import { BoundViewFactory, ViewSlot, customAttribute, templateController, inject } from 'aurelia-framework';

@customAttribute('items-control-2')
@templateController
@inject(BoundViewFactory, ViewSlot)
export class ItemsControl2 {
  constructor(viewFactory, viewSlot) {
    this.viewFactory = viewFactory;
    this.viewSlot = viewSlot;
    this.viewInstances = [];
  }

  valueChanged(newValue) {
    // remove existing instances from viewslot and unbind them
    this.viewInstances.forEach( view => {
      this.viewSlot.remove(view);
      view.unbind();
    });

    this.viewInstances = [];

     // iterate newValue, produce new view instances and put those in the view slot
     newValue.forEach(value => {
       let view = this.viewFactory.create();
       view.bind(value);
       this.viewSlot.add(view);
     });
  }
}
