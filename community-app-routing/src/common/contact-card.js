import {bindable, inject} from 'aurelia-framework';

@inject(Element)
export class ContactCard {
  @bindable contactFirstName

  constructor(element) {
    console.log('Contact constructed');
    this.element = element;
  }

  created(view) {
    console.log('Contact created');
  }

  bind(bidingContext, overrideContext) {
    console.log('Contact bind');
  }

  attached() {
    console.log('Contact attached');
    let fooInput = this.element.querySelector('.foo');
    console.log(fooInput);
  }

  unbind() {
    console.log('Contact unbind');
  }

  detached() {
    console.log('Contact detached');
  }

}
