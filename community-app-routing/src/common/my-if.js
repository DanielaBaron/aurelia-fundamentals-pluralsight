/**
 * A simplified version of Aurelia's' if binding, to demonstrate template controller.
 */

import { BoundViewFactory, ViewSlot, customAttribute, templateController, inject } from 'aurelia-framework';

@customAttribute('my-if')
@templateController
@inject(BoundViewFactory, ViewSlot)
export class If {
  constructor(viewFactory, viewSlot) {
    this.viewFactory = viewFactory;
    this.viewSlot = viewSlot;
    this.showing = false;
  }

  bind(bindingContext) {
    this.bindingContext = bindingContext;
  }

  valueChanged(newValue) {

    // if falsey, hide the chunk of markup that this attribute is on
    if (!newValue) {
      if (this.view) {
        this.viewSlot.remove(this.view);
        this.view.unbind();
      }
      this.showing = false;
      return;
    }

    // create a view instance
    if (!this.view) {
      this.view = this.viewFactory.create();
    }

    // if truthey, show and bind view
    if (!this.showing) {
      this.showing = true;

      if (!this.view.bound) {
        this.view.bind(this.bindingContext);
      }

      this.viewSlot.add(this.view);
    }
  }
}
