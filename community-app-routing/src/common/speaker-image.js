import {inject, bindable} from 'aurelia-framework';

// the element that the attribute has been attached to gets injected
@inject(Element)
export class SpeakerImageCustomAttribute {
  @bindable imageName;
  @bindable isMvp;

  constructor(element) {
    this.element = element;
  }

  // src is the "source" attribute of the element this attribute is declared on,
  // which assumes this is being used on an image tag.
  imageNameChanged(newValue, oldValue) {
    if (newValue) {
      this.element.src = `images/speakers/${newValue}`;
    }
  }

  isMvpChanged(newValue) {
    if (newValue) {
      var el = document.createElement('div');
      el.innerHTML = 'MVP';
      el.className = 'watermark';
      this.element.parentNode.insertBefore(el, this.element.nextSiblig);
    }
  }

  // bind(bindingContext) {
  //   this.imageNameChanged(this.imageName);
  //   this.isMvpChanged(this.isMvp);
  // }

  // since value is being data bound, make sure its initialized on initial binding
  // bind(bindingContext) {
  //   this.imageNameChanged(this.value);
  // }
}
