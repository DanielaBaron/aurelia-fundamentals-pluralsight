import {containerless, customElement, bindable} from 'aurelia-framework';

// example of naming tag differently than class name
// @customElement('navigation-bar')
@containerless
export class NavBar {
  @bindable router;

  created(view) {

  }

  bind(bidingContext, overrideContext) {

  }

  unbind() {

  }

  attached() {

  }

  detached() {

  }
}
