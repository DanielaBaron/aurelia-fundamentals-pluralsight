export class Sponsors {
  constructor() {

    /**
     * Because the sponsors.html view is binding to this.message,
     * Aurelia rewrites this property with Object defineProperty,
     * and in the set block,
     * Left off at 3:44 of Controlling Data Flow with Data Binding Modes
     */
    this.message = 'Sponsors';
    setTimeout(() => this.message = "Changed after binding", 3000);

    this.mapCollection = new window.Map();
    this.mapCollection.set('a', 'Alpha');
    this.mapCollection.set('b', 'Beta');
    this.mapCollection.set('c', 'Charlie');
    this.mapCollection.set('d', 'Delta');

    this.styleString = 'background: red; padding: 2em;';

    this.styleObject = {
      background: 'green'
    };
  }

  doSomething() {
    console.log(this.message);
  }

  // activate() {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       this.message = 'SPONSORS';
  //       console.log('=== SPONSORS RESOLVE ===');
  //       resolve();
  //     }, 300)
  //   });
  // }
}
