/**
 * Root view model for the application as a whole
 */
export class Hello {
  constructor() {
    this.message = 'Hello Aurelia';
    this.image = 'http://aurelia.io/images/main-logo.svg';
    this.input= 'change me';
  }
}
