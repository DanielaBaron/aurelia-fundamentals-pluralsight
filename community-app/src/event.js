import {inject} from 'aurelia-framework';
// import {DataCache} from './data-cache';

@inject('TheBestCacheEver')
export class Event {
  constructor(theBestCacheEver) {
    this.dataCache = theBestCacheEver;
    this.dataCache.data.push('I was put here by Event');
  }
  activate(bindingContext) {
    this.event = bindingContext;
  }
}
