export class Sponsors {
  constructor() {
    this.message = 'SHOULD NOT SEE THIS';
  }

  activate() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.message = 'SPONSORS';
        console.log('=== SPONSORS RESOLVE ===');
        resolve();
      }, 300)
    });
  }
}
