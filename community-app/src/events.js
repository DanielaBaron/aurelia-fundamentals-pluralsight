import {inject, Lazy, All} from 'aurelia-framework';
import {ImLazy} from './im-lazy';

@inject('TheBestCacheEver', Lazy.of(ImLazy), All.of('SuperPlugin'))
export class Events {
  constructor(dataCache, imLazy, plugins) {
    // this will NOT instantiate imLazy
    this.imLazy = imLazy;

    this.dataCache = dataCache;
    this.dataCache.data.push('I was put here by Events');
    this.events = [
      {id: 1, title: 'Aurelia Fundamentals'},
      {id: 2, title: 'Data-Centric SPAs with BreezeJS'}
    ];

    // do something with plugins
    plugins.forEach((plugin) => {
      plugin.doPluginStuff();
    });
  }

  activate() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.message = 'EVENTS';
        console.log('=== EVENTS RESOLVE ===');
        resolve();
      }, 1)
    });
  }

  createAndUseLazy() {
    console.log('about to use lazy');
    // Notice function invocation
    this.imLazy().doStuff();
  }
}
